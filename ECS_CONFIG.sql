-- MySQL dump 10.13  Distrib 5.1.31, for pc-linux-gnu (i686)
--
-- Host: fmddaq    Database: ECS_CONFIG
-- ------------------------------------------------------
-- Server version	5.1.31-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DETECTORS`
--

DROP TABLE IF EXISTS `DETECTORS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `DETECTORS` (
  `NAME` char(32) NOT NULL,
  `DCS_OBJECT` char(32) DEFAULT NULL,
  `LTU_OBJECT` char(32) DEFAULT NULL,
  `DCS_BRIDGE` tinyint(1) NOT NULL DEFAULT '0',
  `DCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','DCAHI','PCA') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  `PARTITION` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`),
  KEY `PARTITION` (`PARTITION`),
  CONSTRAINT `DETECTORS_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `DETECTORS`
--

LOCK TABLES `DETECTORS` WRITE;
/*!40000 ALTER TABLE `DETECTORS` DISABLE KEYS */;
INSERT INTO `DETECTORS` VALUES ('FMD','FMD_DCS::FMD',NULL,0,'Not running','None',NULL,NULL);
/*!40000 ALTER TABLE `DETECTORS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ENV`
--

DROP TABLE IF EXISTS `ENV`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ENV` (
  `NAME` char(32) NOT NULL,
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ENV`
--

LOCK TABLES `ENV` WRITE;
/*!40000 ALTER TABLE `ENV` DISABLE KEYS */;
INSERT INTO `ENV` VALUES ('DIM_DNS_NODE','fmddaq','Host running DIM domain name server'),('DUMMY_DCS','NO','Use a dummy version of the DCS (YES/NO)'),('DUMMY_HLT','YES','Use a dummy version of the HLT (YES/NO)'),('DUMMY_LTU','NO','Use a dummy version of the LTU (YES/NO)');
/*!40000 ALTER TABLE `ENV` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FILES`
--

DROP TABLE IF EXISTS `FILES`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `FILES` (
  `NAME` char(255) NOT NULL,
  `VALUE` longblob,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `FILES`
--

LOCK TABLES `FILES` WRITE;
/*!40000 ALTER TABLE `FILES` DISABLE KEYS */;
/*!40000 ALTER TABLE `FILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GLOBALS`
--

DROP TABLE IF EXISTS `GLOBALS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GLOBALS` (
  `NAME` char(32) NOT NULL DEFAULT '',
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `GLOBALS`
--

LOCK TABLES `GLOBALS` WRITE;
/*!40000 ALTER TABLE `GLOBALS` DISABLE KEYS */;
INSERT INTO `GLOBALS` VALUES ('DB version','v3','The version of the configuration database');
/*!40000 ALTER TABLE `GLOBALS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS`
--

DROP TABLE IF EXISTS `PARTITIONS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS` (
  `NAME` char(32) NOT NULL,
  `PCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','PCAHI') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS`
--

LOCK TABLES `PARTITIONS` WRITE;
/*!40000 ALTER TABLE `PARTITIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS_STRUCTURE`
--

DROP TABLE IF EXISTS `PARTITIONS_STRUCTURE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS_STRUCTURE` (
  `PARTITION` char(32) NOT NULL,
  `DETECTOR` char(32) NOT NULL,
  `EXCLUDED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PARTITION`,`DETECTOR`),
  KEY `DETECTOR` (`DETECTOR`),
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_2` FOREIGN KEY (`DETECTOR`) REFERENCES `DETECTORS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS_STRUCTURE`
--

LOCK TABLES `PARTITIONS_STRUCTURE` WRITE;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STATUS`
--

DROP TABLE IF EXISTS `STATUS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `STATUS` (
  `ECS_name` char(32) NOT NULL DEFAULT '' COMMENT 'Detector or partition associated to the subsystem',
  `ECS_type` enum('Detector','Partition') NOT NULL DEFAULT 'Detector',
  `ECS_subsystem` enum('DAQ','DCS','HLT','TRG') NOT NULL DEFAULT 'DAQ',
  `Status` enum('Not running','Running','Unreachable') DEFAULT NULL COMMENT 'process status',
  `Operator_type` enum('Unknown','Human Interface','DCA','PCA','None') DEFAULT 'Unknown' COMMENT 'what type controls the process',
  `Operator_id` char(32) DEFAULT NULL COMMENT 'who controls the process',
  `Update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Status modification timestamp',
  PRIMARY KEY (`ECS_subsystem`,`ECS_name`,`ECS_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Status of the subsystem control processes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `STATUS`
--

LOCK TABLES `STATUS` WRITE;
/*!40000 ALTER TABLE `STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STATUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-06-09 18:32:19
-- ==================================================
-- Back-up created on 20100609_2215 by running
--  mysqldump -udaq -pdaq -hfmddaq -i 
-- To restore do
--  echo CREATE DATABASE IF NOT EXISTS ECS_CONFIG | mysql -udaq -pdaq -hfmddaq
--  mysql -udaq -pdaq -hfmddaq < ECS_CONFIG.sql
-- ==================================================

-- MySQL dump 10.13  Distrib 5.1.31, for pc-linux-gnu (i686)
--
-- Host: fmddaq    Database: ECS_CONFIG
-- ------------------------------------------------------
-- Server version	5.1.31-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DETECTORS`
--

DROP TABLE IF EXISTS `DETECTORS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `DETECTORS` (
  `NAME` char(32) NOT NULL,
  `DCS_OBJECT` char(32) DEFAULT NULL,
  `LTU_OBJECT` char(32) DEFAULT NULL,
  `DCS_BRIDGE` tinyint(1) NOT NULL DEFAULT '0',
  `DCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','DCAHI','PCA') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  `PARTITION` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`),
  KEY `PARTITION` (`PARTITION`),
  CONSTRAINT `DETECTORS_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `DETECTORS`
--

LOCK TABLES `DETECTORS` WRITE;
/*!40000 ALTER TABLE `DETECTORS` DISABLE KEYS */;
INSERT INTO `DETECTORS` VALUES ('FMD','FMD_DCS::FMD',NULL,0,'Not running','None',NULL,NULL);
/*!40000 ALTER TABLE `DETECTORS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ENV`
--

DROP TABLE IF EXISTS `ENV`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ENV` (
  `NAME` char(32) NOT NULL,
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ENV`
--

LOCK TABLES `ENV` WRITE;
/*!40000 ALTER TABLE `ENV` DISABLE KEYS */;
INSERT INTO `ENV` VALUES ('DIM_DNS_NODE','fmddaq','Host running DIM domain name server'),('DUMMY_DCS','NO','Use a dummy version of the DCS (YES/NO)'),('DUMMY_HLT','YES','Use a dummy version of the HLT (YES/NO)'),('DUMMY_LTU','NO','Use a dummy version of the LTU (YES/NO)');
/*!40000 ALTER TABLE `ENV` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FILES`
--

DROP TABLE IF EXISTS `FILES`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `FILES` (
  `NAME` char(255) NOT NULL,
  `VALUE` longblob,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `FILES`
--

LOCK TABLES `FILES` WRITE;
/*!40000 ALTER TABLE `FILES` DISABLE KEYS */;
/*!40000 ALTER TABLE `FILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GLOBALS`
--

DROP TABLE IF EXISTS `GLOBALS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GLOBALS` (
  `NAME` char(32) NOT NULL DEFAULT '',
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `GLOBALS`
--

LOCK TABLES `GLOBALS` WRITE;
/*!40000 ALTER TABLE `GLOBALS` DISABLE KEYS */;
INSERT INTO `GLOBALS` VALUES ('DB version','v3','The version of the configuration database');
/*!40000 ALTER TABLE `GLOBALS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS`
--

DROP TABLE IF EXISTS `PARTITIONS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS` (
  `NAME` char(32) NOT NULL,
  `PCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','PCAHI') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS`
--

LOCK TABLES `PARTITIONS` WRITE;
/*!40000 ALTER TABLE `PARTITIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS_STRUCTURE`
--

DROP TABLE IF EXISTS `PARTITIONS_STRUCTURE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS_STRUCTURE` (
  `PARTITION` char(32) NOT NULL,
  `DETECTOR` char(32) NOT NULL,
  `EXCLUDED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PARTITION`,`DETECTOR`),
  KEY `DETECTOR` (`DETECTOR`),
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_2` FOREIGN KEY (`DETECTOR`) REFERENCES `DETECTORS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS_STRUCTURE`
--

LOCK TABLES `PARTITIONS_STRUCTURE` WRITE;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STATUS`
--

DROP TABLE IF EXISTS `STATUS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `STATUS` (
  `ECS_name` char(32) NOT NULL DEFAULT '' COMMENT 'Detector or partition associated to the subsystem',
  `ECS_type` enum('Detector','Partition') NOT NULL DEFAULT 'Detector',
  `ECS_subsystem` enum('DAQ','DCS','HLT','TRG') NOT NULL DEFAULT 'DAQ',
  `Status` enum('Not running','Running','Unreachable') DEFAULT NULL COMMENT 'process status',
  `Operator_type` enum('Unknown','Human Interface','DCA','PCA','None') DEFAULT 'Unknown' COMMENT 'what type controls the process',
  `Operator_id` char(32) DEFAULT NULL COMMENT 'who controls the process',
  `Update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Status modification timestamp',
  PRIMARY KEY (`ECS_subsystem`,`ECS_name`,`ECS_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Status of the subsystem control processes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `STATUS`
--

LOCK TABLES `STATUS` WRITE;
/*!40000 ALTER TABLE `STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STATUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-06-09 20:15:48
-- ==================================================
-- Back-up created on 20100609_2226 by running
--  mysqldump -udaq -pdaq -hfmddaq -i 
-- To restore do
--  echo CREATE DATABASE IF NOT EXISTS ECS_CONFIG | mysql -udaq -pdaq -hfmddaq
--  mysql -udaq -pdaq -hfmddaq < ECS_CONFIG.sql
-- ==================================================

-- MySQL dump 10.13  Distrib 5.1.31, for pc-linux-gnu (i686)
--
-- Host: fmddaq    Database: ECS_CONFIG
-- ------------------------------------------------------
-- Server version	5.1.31-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DETECTORS`
--

DROP TABLE IF EXISTS `DETECTORS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `DETECTORS` (
  `NAME` char(32) NOT NULL,
  `DCS_OBJECT` char(32) DEFAULT NULL,
  `LTU_OBJECT` char(32) DEFAULT NULL,
  `DCS_BRIDGE` tinyint(1) NOT NULL DEFAULT '0',
  `DCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','DCAHI','PCA') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  `PARTITION` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`),
  KEY `PARTITION` (`PARTITION`),
  CONSTRAINT `DETECTORS_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `DETECTORS`
--

LOCK TABLES `DETECTORS` WRITE;
/*!40000 ALTER TABLE `DETECTORS` DISABLE KEYS */;
INSERT INTO `DETECTORS` VALUES ('FMD','FMD_DCS::FMD',NULL,0,'Not running','None',NULL,NULL);
/*!40000 ALTER TABLE `DETECTORS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ENV`
--

DROP TABLE IF EXISTS `ENV`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ENV` (
  `NAME` char(32) NOT NULL,
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ENV`
--

LOCK TABLES `ENV` WRITE;
/*!40000 ALTER TABLE `ENV` DISABLE KEYS */;
INSERT INTO `ENV` VALUES ('DIM_DNS_NODE','fmddaq','Host running DIM domain name server'),('DUMMY_DCS','NO','Use a dummy version of the DCS (YES/NO)'),('DUMMY_HLT','YES','Use a dummy version of the HLT (YES/NO)'),('DUMMY_LTU','NO','Use a dummy version of the LTU (YES/NO)');
/*!40000 ALTER TABLE `ENV` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FILES`
--

DROP TABLE IF EXISTS `FILES`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `FILES` (
  `NAME` char(255) NOT NULL,
  `VALUE` longblob,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `FILES`
--

LOCK TABLES `FILES` WRITE;
/*!40000 ALTER TABLE `FILES` DISABLE KEYS */;
/*!40000 ALTER TABLE `FILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GLOBALS`
--

DROP TABLE IF EXISTS `GLOBALS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GLOBALS` (
  `NAME` char(32) NOT NULL DEFAULT '',
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `GLOBALS`
--

LOCK TABLES `GLOBALS` WRITE;
/*!40000 ALTER TABLE `GLOBALS` DISABLE KEYS */;
INSERT INTO `GLOBALS` VALUES ('DB version','v3','The version of the configuration database');
/*!40000 ALTER TABLE `GLOBALS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS`
--

DROP TABLE IF EXISTS `PARTITIONS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS` (
  `NAME` char(32) NOT NULL,
  `PCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','PCAHI') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS`
--

LOCK TABLES `PARTITIONS` WRITE;
/*!40000 ALTER TABLE `PARTITIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS_STRUCTURE`
--

DROP TABLE IF EXISTS `PARTITIONS_STRUCTURE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS_STRUCTURE` (
  `PARTITION` char(32) NOT NULL,
  `DETECTOR` char(32) NOT NULL,
  `EXCLUDED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PARTITION`,`DETECTOR`),
  KEY `DETECTOR` (`DETECTOR`),
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_2` FOREIGN KEY (`DETECTOR`) REFERENCES `DETECTORS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS_STRUCTURE`
--

LOCK TABLES `PARTITIONS_STRUCTURE` WRITE;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STATUS`
--

DROP TABLE IF EXISTS `STATUS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `STATUS` (
  `ECS_name` char(32) NOT NULL DEFAULT '' COMMENT 'Detector or partition associated to the subsystem',
  `ECS_type` enum('Detector','Partition') NOT NULL DEFAULT 'Detector',
  `ECS_subsystem` enum('DAQ','DCS','HLT','TRG') NOT NULL DEFAULT 'DAQ',
  `Status` enum('Not running','Running','Unreachable') DEFAULT NULL COMMENT 'process status',
  `Operator_type` enum('Unknown','Human Interface','DCA','PCA','None') DEFAULT 'Unknown' COMMENT 'what type controls the process',
  `Operator_id` char(32) DEFAULT NULL COMMENT 'who controls the process',
  `Update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Status modification timestamp',
  PRIMARY KEY (`ECS_subsystem`,`ECS_name`,`ECS_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Status of the subsystem control processes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `STATUS`
--

LOCK TABLES `STATUS` WRITE;
/*!40000 ALTER TABLE `STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STATUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-06-09 20:26:36
-- ==================================================
-- Back-up created on 20100609_2226 by running
--  mysqldump -udaq -pdaq -hfmddaq -i 
-- To restore do
--  echo CREATE DATABASE IF NOT EXISTS ECS_CONFIG | mysql -udaq -pdaq -hfmddaq
--  mysql -udaq -pdaq -hfmddaq < ECS_CONFIG.sql
-- ==================================================

-- MySQL dump 10.13  Distrib 5.1.31, for pc-linux-gnu (i686)
--
-- Host: fmddaq    Database: ECS_CONFIG
-- ------------------------------------------------------
-- Server version	5.1.31-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DETECTORS`
--

DROP TABLE IF EXISTS `DETECTORS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `DETECTORS` (
  `NAME` char(32) NOT NULL,
  `DCS_OBJECT` char(32) DEFAULT NULL,
  `LTU_OBJECT` char(32) DEFAULT NULL,
  `DCS_BRIDGE` tinyint(1) NOT NULL DEFAULT '0',
  `DCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','DCAHI','PCA') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  `PARTITION` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`),
  KEY `PARTITION` (`PARTITION`),
  CONSTRAINT `DETECTORS_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `DETECTORS`
--

LOCK TABLES `DETECTORS` WRITE;
/*!40000 ALTER TABLE `DETECTORS` DISABLE KEYS */;
INSERT INTO `DETECTORS` VALUES ('FMD','FMD_DCS::FMD',NULL,0,'Not running','None',NULL,NULL);
/*!40000 ALTER TABLE `DETECTORS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ENV`
--

DROP TABLE IF EXISTS `ENV`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ENV` (
  `NAME` char(32) NOT NULL,
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ENV`
--

LOCK TABLES `ENV` WRITE;
/*!40000 ALTER TABLE `ENV` DISABLE KEYS */;
INSERT INTO `ENV` VALUES ('DIM_DNS_NODE','fmddaq','Host running DIM domain name server'),('DUMMY_DCS','NO','Use a dummy version of the DCS (YES/NO)'),('DUMMY_HLT','YES','Use a dummy version of the HLT (YES/NO)'),('DUMMY_LTU','NO','Use a dummy version of the LTU (YES/NO)');
/*!40000 ALTER TABLE `ENV` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FILES`
--

DROP TABLE IF EXISTS `FILES`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `FILES` (
  `NAME` char(255) NOT NULL,
  `VALUE` longblob,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `FILES`
--

LOCK TABLES `FILES` WRITE;
/*!40000 ALTER TABLE `FILES` DISABLE KEYS */;
/*!40000 ALTER TABLE `FILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GLOBALS`
--

DROP TABLE IF EXISTS `GLOBALS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GLOBALS` (
  `NAME` char(32) NOT NULL DEFAULT '',
  `VALUE` text,
  `DESCRIPTION` text,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `GLOBALS`
--

LOCK TABLES `GLOBALS` WRITE;
/*!40000 ALTER TABLE `GLOBALS` DISABLE KEYS */;
INSERT INTO `GLOBALS` VALUES ('DB version','v3','The version of the configuration database');
/*!40000 ALTER TABLE `GLOBALS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS`
--

DROP TABLE IF EXISTS `PARTITIONS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS` (
  `NAME` char(32) NOT NULL,
  `PCA_STATUS` enum('Not running','Running') NOT NULL DEFAULT 'Not running',
  `OPERATOR_TYPE` enum('None','PCAHI') NOT NULL DEFAULT 'None',
  `OPERATOR_ID` char(32) DEFAULT NULL,
  PRIMARY KEY (`NAME`),
  KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS`
--

LOCK TABLES `PARTITIONS` WRITE;
/*!40000 ALTER TABLE `PARTITIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PARTITIONS_STRUCTURE`
--

DROP TABLE IF EXISTS `PARTITIONS_STRUCTURE`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PARTITIONS_STRUCTURE` (
  `PARTITION` char(32) NOT NULL,
  `DETECTOR` char(32) NOT NULL,
  `EXCLUDED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PARTITION`,`DETECTOR`),
  KEY `DETECTOR` (`DETECTOR`),
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_1` FOREIGN KEY (`PARTITION`) REFERENCES `PARTITIONS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PARTITIONS_STRUCTURE_ibfk_2` FOREIGN KEY (`DETECTOR`) REFERENCES `DETECTORS` (`NAME`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PARTITIONS_STRUCTURE`
--

LOCK TABLES `PARTITIONS_STRUCTURE` WRITE;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PARTITIONS_STRUCTURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STATUS`
--

DROP TABLE IF EXISTS `STATUS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `STATUS` (
  `ECS_name` char(32) NOT NULL DEFAULT '' COMMENT 'Detector or partition associated to the subsystem',
  `ECS_type` enum('Detector','Partition') NOT NULL DEFAULT 'Detector',
  `ECS_subsystem` enum('DAQ','DCS','HLT','TRG') NOT NULL DEFAULT 'DAQ',
  `Status` enum('Not running','Running','Unreachable') DEFAULT NULL COMMENT 'process status',
  `Operator_type` enum('Unknown','Human Interface','DCA','PCA','None') DEFAULT 'Unknown' COMMENT 'what type controls the process',
  `Operator_id` char(32) DEFAULT NULL COMMENT 'who controls the process',
  `Update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Status modification timestamp',
  PRIMARY KEY (`ECS_subsystem`,`ECS_name`,`ECS_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Status of the subsystem control processes';
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `STATUS`
--

LOCK TABLES `STATUS` WRITE;
/*!40000 ALTER TABLE `STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STATUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-06-09 20:26:45
