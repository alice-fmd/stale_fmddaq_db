#
#
#
MYSQLDUMP	=  mysqldump
MYSQL		=  mysql
FLAGS		=  -i 
DB_USER		=  daq
DB_PASSWD	=  daq
DB_HOST		=  fmddaq
ifeq ($(DATE), )
DATE		:= $(shell date +%Y%m%d_%H%M)
endif

getdate		= $(shell grep "\-\- Dump completed" $(1) | tail -n 1 | \
		     sed 's/.* on \([0-9]*\)-\([0-9]*\)-\([0-9]*\) \([0-9]*\):\([0-9]*\):.*/\1\2\3_\4\5/')
DUMP		=  $(MYSQLDUMP) -u$(DB_USER) -p$(DB_PASSWD) -h$(DB_HOST)
CONNECT		=  $(MYSQL) -u$(DB_USER) -p$(DB_PASSWD) -h$(DB_HOST)

%.sql:	FORCE
	@echo "-- =================================================="	>> $@
	@echo "-- Back-up created on $(DATE) by running" 		>> $@
	@echo "--  $(DUMP) $(FLAGS)"  					>> $@ 
	@echo "-- To restore do"                			>> $@ 
	@echo "--  echo CREATE DATABASE IF NOT EXISTS $* | $(CONNECT)" 	>> $@
	@echo "--  $(CONNECT) < $@"					>> $@
	@echo "-- =================================================="	>> $@
	@echo ""							>> $@
	$(DUMP) $(FLAGS) $* >> $@

%.imp:%.sql 
	d=$(call getdate, $<) ; \
		echo "DROP DATABASE IF EXISTS $*_$$d" | $(CONNECT); \
		echo "CREATE DATABASE IF NOT EXISTS $*_$$d" | $(CONNECT); \
		$(CONNECT) $*_$$d < $< 	> $@

backup:	DATE_CONFIG.sql ECS_CONFIG.sql 

import: DATE_CONFIG.imp ECS_CONFIG.imp

FORCE:	

.PHONY:	backup import 


#
# EOF
#
